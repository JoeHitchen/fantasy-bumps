# Generated by Django 2.2 on 2019-11-19 18:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fantasy', '0002_athletes'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='event',
            name='name',
        ),
        migrations.AddField(
            model_name='event',
            name='series',
            field=models.CharField(choices=[('D', 'Demo'), ('T', 'Torpids'), ('E', 'Eights')], db_index=True, default='D', max_length=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='event',
            name='year',
            field=models.PositiveSmallIntegerField(db_index=True, default=2019),
            preserve_default=False,
        ),
    ]
