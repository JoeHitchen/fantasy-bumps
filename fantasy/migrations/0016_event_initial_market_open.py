# Generated by Django 4.2.13 on 2025-02-24 21:45
from datetime import datetime, timedelta
import zoneinfo

from django.db import migrations, models
from django.apps.registry import Apps
from django.db.backends.base.schema import BaseDatabaseSchemaEditor as SchemaEditor

from core.settings import TIME_ZONE

from ..constants import timings


def add_initial_market_opens(apps: Apps, schema_editor: SchemaEditor) -> None:
    """Populates the historical last race times."""
    Event = apps.get_model('fantasy', 'Event')

    events = Event.objects.all()
    for event in events:
        event.initial_market_open = datetime.combine(
            event.days.first().date - timedelta(3),
            timings.MARKET_INITIAL,
            tzinfo = zoneinfo.ZoneInfo(TIME_ZONE),
        )
        event.save()


def noop_reverser(apps: Apps, schema_editor: SchemaEditor) -> None:
    pass


class Migration(migrations.Migration):

    dependencies = [
        ('fantasy', '0015_adding_reuben'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='initial_market_open',
            field=models.DateTimeField(null=True),
        ),
        migrations.RunPython(add_initial_market_opens, noop_reverser),
        migrations.AlterField(
            model_name='event',
            name='initial_market_open',
            field=models.DateTimeField(),
        ),
    ]
